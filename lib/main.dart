import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_app_links/flutter_facebook_app_links.dart';
import 'package:webview_flutter/webview_flutter.dart';

const String url = 'https://www.fcase.ru/app.php';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter WebView Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        '/': (_) => const MyHomePage(),
        '/widget': (_) {
          return WillPopScope(
            child: SafeArea(
              child: WebView(
                initialUrl: 'https://www.fcase.ru/app.php',
                javascriptMode: JavascriptMode.unrestricted,
                onWebViewCreated: (WebViewController webViewController) {
                  _controller.complete(webViewController);
                },
                javascriptChannels: <JavascriptChannel>[
                  JavascriptChannel(
                      name: 'Print',
                      onMessageReceived: (JavascriptMessage message) {
                        print(message.message);
                      }),
                ].toSet(),
                gestureNavigationEnabled: true,
              ),
            ),
            onWillPop: () {
              SystemNavigator.pop();
              return null;
            },
          );
        },
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  dynamic deepLink;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('WebView test app'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
          ),
          RaisedButton(
            onPressed: () {
              Navigator.of(context).pushNamed('/widget');
            },
            child: const Text('Privacy Policy'),
          ),
          FutureBuilder(
              future: FlutterFacebookAppLinks.initFBLinks(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  deepLink = snapshot.data;
                  return Text(snapshot.data.toString());
                } else
                  return Text('$deepLink');
              }),
        ],
      ),
    );
  }
}
